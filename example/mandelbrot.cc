/*
 * Copyright (c) 2018 Jack Poulson <jack@hodgestar.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <complex>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
#include "specify.hpp"

using std::complex;

// Iterates using the update
//   z := z * z + z_0
// until |z| >= 2 or the maximum number of iterations is reached.
// The return value is the performed number of iterations.
int MandelbrotCalculate(const complex<double>& z0, int max_iter) {
  complex<double> z = z0;
  int num_iter = 0;
  for (; num_iter < max_iter; ++num_iter) {
    if (std::abs(z) >= 2.) break;
    z = z * z + z0;
  }
  return num_iter;
}

// Prints a legend for the plot.
void PrintLegend(const std::vector<char>& char_grad) {
  std::cout << "ASCII iteration count buckets:\n" << ' ' << ": " << 0 << '\n';
  for (unsigned which_char = 0; which_char < char_grad.size(); ++which_char) {
    std::cout << char_grad[which_char] << ": " << std::exp(which_char) << "-"
              << std::exp(which_char + 1) << '\n';
  }
  std::cout.flush();
}

// Converts an iteration count into the corresponding ASCII character.
char IterationCountChar(int num_iter, const std::vector<char>& char_grad) {
  char iter_count_char = ' ';
  if (num_iter > 0) {
    const int log_num_iter = std::log(1. * num_iter);
    const int which_char =
        std::min(log_num_iter, static_cast<int>(char_grad.size() - 1));
    iter_count_char = char_grad[which_char];
  }
  return iter_count_char;
}

int main(int argc, char** argv) {
  specify::ArgumentParser parser(argc, argv);
  const int max_iter = parser.OptionalInput<int>(
      "max_iter", "The maximum number of fractal iterations.", 100000);
  const int height = parser.OptionalInput<int>(
      "height", "The number of vertical pixels in the rendering.", 50);
  const int width = parser.OptionalInput<int>(
      "width", "The number of horizontal pixels in the rendering.", 78);
  const double x_center = parser.OptionalInput<double>(
      "x_center", "The horizontal center of the rendering.", -0.7);
  const double y_center = parser.OptionalInput<double>(
      "y_center", "The vertical center of the rendering.", 0.);
  const double x_span = parser.OptionalInput<double>(
      "x_span", "The horizontal span of the rendering.", 2.7);
  if (!parser.OK()) {
    return 0;
  }

  const double aspect_ratio = ((4. / 3.) * height) / width;
  const complex<double> center(x_center, y_center),
      span(x_span, aspect_ratio * x_span);
  const complex<double> begin = center - span / 2.;

  // A sequence of characters which (roughly) progressively fills up more
  // screen space.
  const std::vector<char> char_grad{'.', ',', 'c', 'j', 'a', 'w', 'r', 'p', 'o',
                                    'g', 'J', 'O', 'P', 'Q', 'G', 'E', 'M'};
  PrintLegend(char_grad);

  for (int pixel = 0; pixel < height * width; ++pixel) {
    const int x = pixel % width, y = pixel / width;

    // Convert the integer coordinates to their Cartesian coordinates.
    const complex<double> offset(x * span.real() / (width + 1.),
                                 y * span.imag() / (height + 1.));
    const int num_iter = MandelbrotCalculate(begin + offset, max_iter);
    const char iter_count_char = IterationCountChar(num_iter, char_grad);

    // Print this pixel (and the right border if it follows).
    std::putchar(iter_count_char);
    if (x + 1 == width) std::puts("|");
  }

  return 0;
}
